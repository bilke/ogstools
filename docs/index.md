
```{eval-rst}
:html_theme.sidebar_secondary.remove: true
```

# Contents

```{toctree}
---
maxdepth: 1
---
Development <development>
```

---

```{toctree}
---
maxdepth: 3
---
reference/ogstools
```

---

```{include} ../README.md
```

---

# Indices and tables

* {ref}`genindex`
* {ref}`modindex`
* {ref}`search`
